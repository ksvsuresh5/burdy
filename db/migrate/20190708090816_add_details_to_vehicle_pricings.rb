class AddDetailsToVehiclePricings < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicle_pricings, :base_fare, :float
    add_column :vehicle_pricings, :minimum_fare, :float
    add_column :vehicle_pricings, :price_per_min, :float
  end
end
