class CreateDrivers < ActiveRecord::Migration[5.1]
  def change
    create_table :drivers do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :password_digest
      t.string :mobile
      t.string :reset_password_token
      t.boolean :active
      t.string :session_token
      t.boolean :approved
      t.string :location

      t.timestamps
    end
  end
end
