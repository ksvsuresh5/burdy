class RemoveOwnerFromVehicles < ActiveRecord::Migration[5.2]
  def change
    remove_column :vehicles, :owner_aadhar_front
    remove_column :vehicles, :owner_aadhar_back
    remove_column :vehicles, :owner_pan
  end
end
