class AddLocTypeToUserLocations < ActiveRecord::Migration[5.2]
  def change
    add_column :user_locations, :location_type, :integer
    remove_column :user_locations, :type, :integer
  end
end
