class ModifyTypeInVehiclePricings < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicle_pricings, :vehicle_type, :string
    remove_column :vehicle_pricings, :type, :string
  end
end
