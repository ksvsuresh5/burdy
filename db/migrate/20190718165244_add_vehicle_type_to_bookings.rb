class AddVehicleTypeToBookings < ActiveRecord::Migration[5.2]
  def change
    add_column :bookings, :vehicle_type, :integer
  end
end
