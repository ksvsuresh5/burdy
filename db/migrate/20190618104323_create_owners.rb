class CreateOwners < ActiveRecord::Migration[5.2]
  def change
    create_table :owners do |t|
      t.string :name
      t.string :email
      t.string :aadhar_front
      t.string :aadhar_back
      t.string :pan
      t.string :account_number
      t.string :ifsc
      t.string :passbook

      t.timestamps
    end
  end
end
