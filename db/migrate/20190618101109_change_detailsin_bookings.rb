class ChangeDetailsinBookings < ActiveRecord::Migration[5.2]
  def change
    remove_column :bookings, :pickup_loc
    remove_column :bookings, :drop_loc
    add_column :bookings, :pickup_lat, :string
    add_column :bookings, :pickup_long, :string
    add_column :bookings, :drop_lat, :string	
    add_column :bookings, :drop_long, :string
    add_column :bookings, :distance, :string
    add_column :bookings, :arrival_time, :datetime
    add_column :bookings, :cancel_reason, :string
    add_column :bookings, :toll_amount, :float
  end
end
