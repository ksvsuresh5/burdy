class CreateVehicles < ActiveRecord::Migration[5.1]
  def change
    create_table :vehicles do |t|
      t.integer :driver_id
      t.string :type
      t.string :make
      t.string :model
      t.string :number
      t.string :registration_certificate
      t.string :insurance_number
      t.string :insurance_img
      t.string :permit
      t.string :permit_img
      t.string :color
      t.string :owner_aadhar_front
      t.string :owner_aadhar_back
      t.string :owner_pan
      t.boolean :active

      t.timestamps
    end
  end
end
