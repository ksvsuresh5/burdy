class CreateOtps < ActiveRecord::Migration[5.1]
  def change
    create_table :otps do |t|
      t.string :mobile
      t.string :otp
      t.integer :num_of_tries

      t.timestamps
    end
  end
end
