class CreateDriverDocs < ActiveRecord::Migration[5.1]
  def change
    create_table :driver_docs do |t|
      t.integer :driver_id
      t.string :aadhar_number
      t.string :aadhar_front
      t.string :aadhar_back
      t.string :dl_number
      t.string :dl_front
      t.string :dl_back
      t.string :pan_number
      t.string :pan_img

      t.timestamps
    end
  end
end
