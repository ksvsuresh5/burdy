class ModifyPriceInVehiclePricings < ActiveRecord::Migration[5.2]
  def change
    remove_column :vehicle_pricings, :price_per_km
    add_column :vehicle_pricings, :price_per_km, :float
    remove_column :vehicle_pricings, :waiting_price_per_min
    add_column :vehicle_pricings, :waiting_price_per_min, :float
  end
end
