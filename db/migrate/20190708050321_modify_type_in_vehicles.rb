class ModifyTypeInVehicles < ActiveRecord::Migration[5.2]
  def change
    remove_column :vehicle_pricings, :price_per_km
    add_column :vehicle_pricings, :price_per_km, :string
    remove_column :vehicle_pricings, :waiting_price_per_min
    add_column :vehicle_pricings, :waiting_price_per_min, :string
    remove_column :vehicles, :type
  end
end
