class CreateBookings < ActiveRecord::Migration[5.1]
  def change
    create_table :bookings do |t|
      t.integer :user_id
      t.integer :driver_id
      t.datetime :pickup_time
      t.datetime :drop_time
      t.string :pickup_loc
      t.string :drop_loc
      t.string :path
      t.string :status
      t.float :price
      t.float :discount
      t.float :cgst
      t.float :sgst
      t.float :final_price
      t.string :payment_method
      t.string :payment_status

      t.timestamps
    end
  end
end
