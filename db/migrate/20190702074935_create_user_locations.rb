class CreateUserLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :user_locations do |t|
      t.integer :user_id
      t.string :location_name
      t.string :lat
      t.string :long

      t.timestamps
    end
  end
end
