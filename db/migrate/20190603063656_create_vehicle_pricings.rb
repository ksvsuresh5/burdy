class CreateVehiclePricings < ActiveRecord::Migration[5.1]
  def change
    create_table :vehicle_pricings do |t|
      t.string :type
      t.integer :price_per_km
      t.integer :waiting_price_per_min
      t.boolean :active

      t.timestamps
    end
  end
end
