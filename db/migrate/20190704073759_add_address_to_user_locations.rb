class AddAddressToUserLocations < ActiveRecord::Migration[5.2]
  def change
    add_column :user_locations, :address, :string
  end
end
