class ChangeDetailsinDrivers < ActiveRecord::Migration[5.2]
  def change
    add_column :drivers, :online, :boolean
    add_column :drivers, :vehicle_id, :integer
    add_column :drivers, :owner_id, :integer
    remove_column :vehicles, :driver_id
    add_column :vehicles, :vehicle_pricing_id, :integer
  end
end
