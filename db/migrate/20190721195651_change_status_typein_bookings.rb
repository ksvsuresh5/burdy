class ChangeStatusTypeinBookings < ActiveRecord::Migration[5.2]
  def change
    remove_column :bookings, :status
    add_column :bookings, :status, :integer
    add_column :drivers, :rating, :float
    add_column :drivers, :picture_url, :string
    add_column :vehicles, :picture_url, :string

  end
end
