class CreateDriverLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :driver_locations do |t|
      t.integer :driver_id
      t.string :location_name
      t.string :lat
      t.string :long

      t.timestamps
    end
  end
end
