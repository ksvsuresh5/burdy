class CreateSubscriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :subscriptions do |t|
      t.integer :driver_id
      t.integer :subscription_plan_id
      t.float :amount
      t.string :status
      t.date :valid_till

      t.timestamps
    end
  end
end
