class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :fb_id
      t.string :fb_name
      t.string :fb_email
      t.string :goo_id
      t.string :goo_name
      t.string :goo_email
      t.string :mobile
      t.string :password_digest
      t.string :reset_password_token
      t.boolean :active
      t.string :session_token

      t.timestamps
    end
  end
end
