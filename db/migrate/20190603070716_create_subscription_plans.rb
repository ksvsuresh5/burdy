class CreateSubscriptionPlans < ActiveRecord::Migration[5.1]
  def change
    create_table :subscription_plans do |t|
      t.string :plan_type
      t.float :value
      t.boolean :active

      t.timestamps
    end
  end
end
