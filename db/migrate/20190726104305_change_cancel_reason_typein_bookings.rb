class ChangeCancelReasonTypeinBookings < ActiveRecord::Migration[5.2]
  def change
    remove_column :bookings, :cancel_reason
    add_column :bookings, :cancel_reason, :integer
  end
end
