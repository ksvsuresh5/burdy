class AddCapacityToVehiclePricings < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicle_pricings, :capacity, :integer
  end
end
