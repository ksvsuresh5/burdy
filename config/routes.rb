Rails.application.routes.draw do
  
  namespace :api do
    namespace :v1 do
      namespace :users do
        resources :auth do
          collection do
            post 'google'
            post 'facebook'
            post 'profile'
            post 'login'
            get 'mobile_verified'
            post 'verify_otp'
            post 'gcmtoken'
            get 'profile'
            get 'referral'
            get 'referred'
            post 'send_otp'
            post 'forgot_password_otp'
            post 'forgot_password_otp_verify'
            post 'reset_password'
          end
        end
        resources :location do
          collection do
            
          end
        end
        resources :dashboard do
          collection do
            post 'drivers_nearby'
            post 'cab_details'
          end
        end
        resources :booking do
          collection do
            get 'cancel_list'
          end
        end

      end

      namespace :drivers do
        resources :auth do
          collection do
            post 'profile'
            post 'login'
            post 'verify_otp'
            post 'gcmtoken'
            get 'profile'
            get 'referral'
            get 'referred'
            post 'send_otp'
            post 'forgot_password'
            post 'reset_password'
          end
        end
        resources :dashboard do
          collection do
            post 'location'
          end
        end
        resources :booking do
          collection do
            post 'accept'
            post 'decline'
          end
        end

      end
    end
  end

  namespace :admin do
    resources :booking do
      collection do
        
      end
    end
  end

  root 'admin/bookings#index'

  mount ActionCable.server => '/cable'
end
