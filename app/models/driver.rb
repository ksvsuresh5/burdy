class Driver < ApplicationRecord
  has_many :subscriptions
  has_many :bookings
  has_many :driver_locations
  has_one :vehicle
  has_one :driver_doc
  has_one_attached :picture
  validates :email, uniqueness: true, uniqueness: { case_sensitive: false }
  validates :mobile, uniqueness: true
  before_create :generate_session_token
  has_secure_password validations: false

  private
	def generate_session_token
		loop do
      self.session_token = SecureRandom.base64.tr('+/=', 'Qrt')
      break self.session_token unless Driver.exists?(session_token: self.session_token)
    end
	end

end
