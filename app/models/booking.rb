class Booking < ApplicationRecord
  belongs_to :user
  belongs_to :driver, optional: true

  enum status: [:processing,:accepted,:no_drivers_available,:arriving,:in_progress,:driver_cancelled,:rider_cancelled,:completed]
  enum cancel_reason: [:'Booked another cab',:'Shorter wait time',:'Unable to contact driver',:'Driver asked to cancel',:'Not listed']
end
