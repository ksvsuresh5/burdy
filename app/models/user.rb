class User < ApplicationRecord
  has_many :bookings , foreign_key: :user_id
  has_many :user_locations
  validates :user_email, uniqueness: true, uniqueness: { case_sensitive: false }, allow_nil: true
  validates :mobile, uniqueness: true, allow_nil: true
  before_create :generate_session_token
  has_secure_password validations: false

  def fetch_fb_data(access_token)
		@graph   = Koala::Facebook::API.new(access_token)
		profile  = @graph.api("me?fields=id,name,email")
      if profile["email"].nil?
      self.update(fb_email: profile["email"], fb_id: profile["id"], fb_name: profile["name"])
      else
      goo_user = User.find_by_goo_email(profile["email"])
      user = goo_user.nil? ? self : goo_user
      user.update(fb_email: profile["email"], fb_id: profile["id"], fb_name: profile["name"])
      end
		return user
	end

	def fetch_google_data(access_token)
		profile = HTTParty.get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=#{access_token}")
		if profile["email"].nil?
    self.update(goo_email: profile["email"], goo_name: profile["name"], goo_id: profile["sub"])
    else
    fb_user = User.find_by_fb_email(profile["email"])
    user = fb_user.nil? ? self : fb_user
    user.update(goo_email: profile["email"], goo_name: profile["name"], goo_id: profile["sub"])
    end
		return user
  end

  def generate_password_token!
    self.reset_password_token = '3456'
    self.reset_password_sent_at = Time.now.utc
    save!
   end
   
   def password_token_valid?
    (self.reset_password_sent_at + 20.minutes) > Time.now.utc
   end
   
   def reset_password!(password)
    self.reset_password_token = nil
    self.password = password
    save!
   end
  
  private
	def generate_session_token
		loop do
      self.session_token = SecureRandom.base64.tr('+/=', 'Qrt')
      break self.session_token unless User.exists?(session_token: self.session_token)
    end
  end


end
