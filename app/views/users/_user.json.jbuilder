json.extract! user, :id, :fb_id, :fb_name, :fb_email, :goo_id, :goo_name, :goo_email, :mobile, :password_digest, :reset_password_token, :active, :session_token, :created_at, :updated_at
json.url user_url(user, format: :json)
