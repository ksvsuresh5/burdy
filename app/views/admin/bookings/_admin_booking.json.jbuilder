json.extract! admin_booking, :id, :created_at, :updated_at
json.url admin_booking_url(admin_booking, format: :json)
