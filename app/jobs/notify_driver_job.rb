class NotifyDriverJob < ApplicationJob
  queue_as :default

  def perform(drivers, user_id)
    ActionCable.server.broadcast("user_channel", {drivers: drivers, user_id: user_id})
  end
end
