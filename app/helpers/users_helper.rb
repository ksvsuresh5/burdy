module UsersHelper
    def get_eta(origin, destination)
        url = "https://maps.googleapis.com/maps/api/distancematrix/json?"
            response = HTTParty.post(url, 
              query: { 
                origins: origin, 
                destinations: destination,
                departure_time: "now",
                key: "AIzaSyAxmUzpsm3afSBr8GhJrnJogfHbjixkb1o"
               })
            eta = response['rows'][0]['elements'][0]['duration_in_traffic']['text'].split(' ')[0]
            return eta
      end
end
