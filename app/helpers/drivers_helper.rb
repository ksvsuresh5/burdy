module DriversHelper
    
    def get_drivers_redis (lat, long)      
        drivers_list = Rails.cache.redis.georadius('driver_location', long, lat, 2,'km','WITHCOORD')
        drivers = []
        
        drivers_list.each do |item|
            driver = {}
            driver['vehicle_id'] = Driver.find(item[0]).vehicle_id
            driver['lat'] = item[1][1]
            driver['long'] = item[1][0]
            driver['heading'] = ''
            drivers.push(driver)   
        end
        
        render json: {  success: true, message: "successful", status_code: 200, data: drivers}
    end

    def nearby_drivers_redis (lat, long)      
        drivers_list = Rails.cache.redis.georadius('driver_location', long, lat, 2,'km','WITHCOORD')
          drivers = []
        
          drivers_list.each do |item|
            drivers.push(item[0])   
        end
        return drivers
    end

    def set_drivers_redis (id, lat, long)
        Rails.cache.redis.geoadd('driver_location', long, lat, id)
        Rails.cache.redis.expire('driver_location',24.hour.to_i)
        render json: {  success: true, message: "successful", status_code: 200, data: {}}
    end

end