class UserChannel < ApplicationCable::Channel
  def subscribed
    stream_from "user_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def nearby_driver(data)
    data['drivers'].each do |item|
      ActionCable.server.broadcast("driver_#{item}", { user_id: data['user_id'] })
    end

  end  
end
