class BookingChannel < ApplicationCable::Channel
  def subscribed
    stream_from "booking_#{params[:booking_id]}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
