class DriverLocationChannel < ApplicationCable::Channel
  include UsersHelper

  def subscribed
    stream_from "driver_location_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def add_location(data)
    @lat = data['driver_location']['lat']
    @long = data['driver_location']['long']
    Rails.cache.redis.geoadd('driver_location', @long, @lat, data['driver_location']['driver_id'] )
    Rails.cache.redis.expire('driver_location',24.hour.to_i)
    @booking = Booking.find(data['booking_id'])   
    @status = @booking.status
    @origin = @lat + ',' + @long
    @destination = @booking.pickup_lat + ',' + @booking.pickup_long
    @eta = get_eta(@origin, @destination)
    ActionCable.server.broadcast("booking_#{data['booking_id']}", { lat: @lat, long: @long, driver_id: data['driver_location']['driver_id'], status:@status, eta: @eta })
  end  
end
