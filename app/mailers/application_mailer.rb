class ApplicationMailer < ActionMailer::Base
  default from: 'sureshk@goscale.co'
  layout 'mailer'
end
