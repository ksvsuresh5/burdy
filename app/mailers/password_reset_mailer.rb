class PasswordResetMailer < ApplicationMailer
    def reset_password_email(email, token)
        @email = email
        @token = token
        mail(to: email, subject: 'Password Reset Link')
      end
end
