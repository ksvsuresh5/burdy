App.driver_location = App.cable.subscriptions.create "DriverLocationChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    @perform 'add_location', driver_location: data.driver_location, booking_id: data.booking_id
