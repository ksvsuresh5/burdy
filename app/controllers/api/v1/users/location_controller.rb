class Api::V1::Users::LocationController < ApplicationController
    before_action :authenticate_user, only: [:create, :index]
    skip_before_action :verify_authenticity_token  

  
      def create
        if user_params.present?
            begin
                @loc = UserLocation.find_or_initialize_by(user_id: @current_user.id, location_name: params[:location_name])
                @loc.update(user_params)
                render json: {  success: true, message: "Location saved successfully", status_code: 201, data: {}}     
            rescue
                render json: {  success: false, message: "Failed to save location", status_code: 500, data: {}}
            end
                 
        else
        render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
        end
      end

      def index
      @user_loc = UserLocation.where(user_id: @current_user.id)
      render json: {  success: true, message: "success", status_code: 200, data: @user_loc}
      end
  
  
  end
  