class Api::V1::Users::BookingController < ApplicationController
    before_action :authenticate_user, only: [:create,:show,:destroy,:cancel_list,:update]
    skip_before_action :verify_authenticity_token  
    include DriversHelper

      def create
        if booking_params.present?
          begin
            @drivers = nearby_drivers_redis(params[:pickup_lat], params[:pickup_long])
            NotifyDriverJob.perform_now(@drivers, @current_user.id)
            @booking = Booking.create(user_id: @current_user.id, status: 1, driver_id: 3)
            @booking.update(booking_params)
            render json: {  success: true, message: "Booking request sent", status_code: 200, data: {booking_id: @booking.id}} 
          rescue
            render json: {  success: false, message: "Booking request failed", status_code: 500, data: {}}
          end 
        else
          render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
        end

      end
      
      def show 
        @booking = Booking.find(params[:id])
        response = {}
        response['status'] = Booking.statuses.fetch(@booking.status)
        destination = {}
        destination['latitude'] = @booking.drop_lat
        destination['longitude'] = @booking.drop_long
        response['destination'] = destination

        pickup = {}
        pickup['latitude'] = @booking.pickup_lat
        pickup['longitude'] = @booking.pickup_long
        response['pickup'] = pickup
        
        if ["processing","no_drivers_available"].exclude? @booking.status
            driver = {}
            @driver = Driver.find(@booking.driver_id)
            driver['name'] = @driver.first_name + ' ' + @driver.last_name
            driver['mobile'] = @driver.mobile
            driver['rating'] = @driver.rating
            driver['picture_url'] = @driver.picture_url || ''
            response['driver'] = driver

            vehicle = {}
            @vehicle = Vehicle.find(@driver.vehicle_id)
            vehicle['make'] = @vehicle.make
            vehicle['model'] = @vehicle.model
            vehicle['license'] = @vehicle.number
            vehicle['picture_url'] = @vehicle.picture_url || ''
            response['vehicle'] = vehicle
        end

        render json: {  success: true, message: "success", status_code: 200, data: response}
      end

      def destroy
        @booking = Booking.find(params[:id])
        @reason = Booking.cancel_reasons.fetch(params[:cancel_reason])
        @booking.update(status: 6, cancel_reason: @reason)
        render json: {  success: true, message: "booking cancelled", status_code: 200, data: {}}
      end

      def cancel_list
        list = Booking.cancel_reasons.keys
        render json: {  success: true, message: "success", status_code: 200, data: list}
      end

      def update
        @booking = Booking.find(params[:id])
        @booking.update(booking_params)
        render json: {  success: true, message: "booking upadted", status_code: 200, data: {}}
      end

      private
      def booking_params
        params.permit(:vehicle_type, :pickup_lat,:pickup_long,:drop_lat,:drop_long)
      end
  
    
  end
  