class Api::V1::Users::AuthController < ApplicationController
    before_action :authenticate_user, only: [:reset_password, :gcmtoken]
    skip_before_action :verify_authenticity_token  

  
      def facebook
          if params[:fb_id]
              @user = User.find_by(fb_id: params[:fb_id],active: true)
        if @user.nil?
          render json: {  success: true, message: "Account not linked", new_user: true, status_code: 200, data: {}}
        else
            render json: {  success: true, message: "FB account already linked", new_user: false, status_code: 200, data: {session_token: @user.session_token}}
        end
      else
        render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
          end
      end
  
    def google
      if params[:goo_id]
        @user = User.find_by(goo_id: params[:goo_id],active: true)
        if @user.nil?
          render json: {  success: true, message: "Account not linked", new_user: true, status_code: 200, data: {}}
        else
            render json: {  success: true, message: "Google account already linked", new_user: false, status_code: 200, data: {session_token: @user.session_token}}
        end
      else
        render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
      end
    end
  
  
    def profile
    if user_params.present?

      if user_params['fb_id'].present?
        begin
          @user = User.create(user_params)
          @user.update(active: true)
          render json: {  success: true, message: "FB user created successfully", new_user: true, status_code: 201, data: {session_token: @user.session_token}}
        rescue
          render json: {  success: false, message: "Failed to create user", status_code: 500, data: {}}
      end
      elsif user_params['goo_id'].present?
        begin
          @user = User.create(user_params)
          @user.update(active: true)
          render json: {  success: true, message: "Google user created successfully", new_user: true, status_code: 201, data: {session_token: @user.session_token}}
        rescue
          render json: {  success: false, message: "Failed to create user", status_code: 500, data: {}}
      end
      else
        begin
            @user = User.create(user_params)
            @user.update(active: true)
            render json: {  success: true, message: "User created successfully", new_user: true, status_code: 201, data: {session_token: @user.session_token}}
        rescue
            render json: {  success: false, message: "Failed to create user", status_code: 500, data: {}}
        end
      end
    else
        render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
    end
   end
  
  
   def login
    if params[:mobile] && params[:password]
      user = User.find_by(mobile: params[:mobile])
      if user && user.authenticate(params[:password])
        render json: {  success: true, message: "Login success", new_user: false, status_code: 200, data: {session_token: user.session_token}}
      else
        render json: {  success: false, message: "Authentication failed", status_code: 412, data: {}}
      end
    else
        render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
    end
   end

      def gcmtoken
        begin
        current_user.update(gcm_params)
        render status: 200, json: { success: true}
        rescue
          render status: 401, json: {message: "gcm update Failed"}
        end
      end
  
    def verify_otp
      otp = '1234'
      if params[:mobile] && params[:otp]
        if otp == params[:otp]
        @user = User.find_by(mobile: params[:mobile])
            if @user.blank?
              render json: {  success: true, message: "Otp verified", status_code: 200, data: {}}
            else
              render json: {  success: true, message: "Login success", status_code: 200, new_user:false, data: {session_token: @user.session_token}}
            end
        else
            render json: {  success: false, message: "Otp verification failed", status_code: 412, data: {}}
        end
      else
        render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
      end
    end
  
    def send_otp
      if params[:mobile]
        otp = '1234'
        @mob = Otp.find_by(mobile: params[:mobile])
        if @mob.blank?
          begin
            @otp = Otp.create(mobile: params[:mobile], otp: otp)
            render json: {  success: true, message: "otp sent successfully", status_code: 200, data: {}}
          rescue
            render json: {  success: false, message: "Failed to send otp", status_code: 500, data: {}}
           end
        else
          @mob.update(otp: otp)
          render json: {  success: true, message: "Resent Otp", status_code: 200, data: {}}
        end

      else
        render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
        
      end
   end
  
  def mobile_verified
    if current_user.mobile_verified
    render status: 200, text: true
    else
    render status: 200, text: false
    end
  end
  
  
  def forgot_password_otp
    if params[:mobile]
      @user = User.find_by(mobile: params[:mobile])
      if @user.present?
          @user.generate_password_token!
        render json: {  success: true, message: "otp sent successfully", status_code: 200, data: {otp: @user.reset_password_token}}
      else
        render json: {  success: false, message: "mobile number not registered with us", status_code: 412, data: {}}
      end
    else
      render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
      end
  end

  def forgot_password_otp_verify
    if params[:mobile] && params[:otp]
      @user  = User.find_by(mobile: params[:mobile])
      if @user.reset_password_token == params[:otp]
        render json: {  success: true, message: "otp verified", status_code: 200, data: {session_token: @user.session_token}}
      else
        render json: {  success: false, message: "verification failed", status_code: 412, data: {}}
      end
    else
      render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
    end
  end
  
  def reset_password
    
    if params[:password] && params[:password_confirm]
      if @current_user.password_token_valid?
        if params[:password] == params[:password_confirm]
          if @current_user.reset_password!(params[:password])
            render json: {  success: true, message: "password reset success", status_code: 200, data: {}}
          else
            render json: {  success: false, message: "password reset failed", status_code: 500, data: {}}
          end
        else
          render json: {  success: false, message: "password mismatch", status_code: 412, data: {}}
        end
      else
        render json: {  success: false, message: "token not valid or expired", status_code: 412, data: {}}
      end
    else
      render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
      end
    end
  
  private
    def gcm_params
      params.permit(:gcm_registration_id,:device_id)
    end
  
    def user_params
      params.permit(:user_name,:user_email,:password,:mobile,:fb_id,:fb_name,:fb_email,:goo_id,:goo_name,:goo_email)
    end
  
  
  end
  