class Api::V1::Users::DashboardController < ApplicationController
    before_action :authenticate_user, only: [:drivers_nearby, :cab_details]
    skip_before_action :verify_authenticity_token  
    include DriversHelper
  
      def drivers_nearby
        @drivers = get_drivers_redis(params[:lat], params[:long])
      end

      def cab_details
        url = "https://maps.googleapis.com/maps/api/distancematrix/json?"
        response = HTTParty.post(url, 
          query: { 
            origins: params[:origin], 
            destinations: params[:destination],
            departure_time: "now",
            key: "AIzaSyAxmUzpsm3afSBr8GhJrnJogfHbjixkb1o"
           })

          burdy_s = VehiclePricing.find_by(vehicle_type: 'Burdy S')
          
          cab_details = []
          burdy_small = {}
          burdy_small['cabType'] = 1
          burdy_small['base_fare'] = burdy_s.base_fare
          burdy_small['minimum_fare'] = burdy_s.minimum_fare
          burdy_small['capacity'] = burdy_s.capacity
          burdy_small['time'] = response['rows'][0]['elements'][0]['duration_in_traffic']['text'].split(' ')[0]
          burdy_small['distance'] = response['rows'][0]['elements'][0]['distance']['text']
          burdy_small['estimated_fare'] = burdy_s.base_fare + (burdy_s.price_per_min * burdy_small['time'].split(' ')[0].to_f ) + (burdy_s.price_per_km * burdy_small['distance'].split(' ')[0].to_f)
          cab_details.push(burdy_small)   
        
          burdy_m = VehiclePricing.find_by(vehicle_type: 'Burdy M')
          burdy_medium = {}
          burdy_medium['cabType'] = 2
          burdy_medium['base_fare'] = burdy_m.base_fare
          burdy_medium['minimum_fare'] = burdy_m.minimum_fare
          burdy_medium['capacity'] = burdy_m.capacity
          burdy_medium['time'] = response['rows'][0]['elements'][0]['duration_in_traffic']['text'].split(' ')[0]
          burdy_medium['distance'] = response['rows'][0]['elements'][0]['distance']['text']
          burdy_medium['estimated_fare'] = burdy_m.base_fare + (burdy_m.price_per_min * burdy_medium['time'].split(' ')[0].to_f ) + (burdy_m.price_per_km * burdy_medium['distance'].split(' ')[0].to_f)
          cab_details.push(burdy_medium)   

          burdy_l = VehiclePricing.find_by(vehicle_type: 'Burdy L')
          burdy_large = {}
          burdy_large['cabType'] = 3
          burdy_large['base_fare'] = burdy_l.base_fare
          burdy_large['minimum_fare'] = burdy_l.minimum_fare
          burdy_large['capacity'] = burdy_l.capacity
          burdy_large['time'] = response['rows'][0]['elements'][0]['duration_in_traffic']['text'].split(' ')[0]
          burdy_large['distance'] = response['rows'][0]['elements'][0]['distance']['text']
          burdy_large['estimated_fare'] = burdy_l.base_fare + (burdy_l.price_per_min * burdy_large['time'].split(' ')[0].to_f ) + (burdy_l.price_per_km * burdy_large['distance'].split(' ')[0].to_f)
          cab_details.push(burdy_large)   

          render json: {  success: true, message: "success", status_code: 200, data: cab_details}
      end
  
    private
    def booking_params
      params.permit(:vehicle_type, :pickup_lat,:pickup_long,:drop_lat,:drop_long)
    end
  
  end
  