class Api::V1::Drivers::BookingController < ApplicationController
    before_action :authenticate_driver, only: [:accept, :decline, :show, :index]
    skip_before_action :verify_authenticity_token  
    include DriversHelper
  
      def accept
        @booking = Booking.find(params[:booking_id])
        @booking.update(status: 1, driver_id: @current_driver.id)
        render json: {  success: true, message: "Booking request accepted", status_code: 200, data: {}} 
      end

      def decline
        @booking = Booking.find(params[:booking_id])
        @booking.update(status: 5)
        render json: {  success: true, message: "Booking request declined", status_code: 200, data: {}} 
      end

      def show 
        @booking = Booking.find(params[:id])
        response = {}
        response['status'] = @booking.status
        destination = {}
        destination['latitude'] = @booking.drop_lat
        destination['longitude'] = @booking.drop_long
        response['destination'] = destination

        pickup = {}
        pickup['latitude'] = @booking.pickup_lat
        pickup['longitude'] = @booking.pickup_long
        response['pickup'] = pickup
        
        
        user = {}
        @user = User.find(@booking.user_id)
        user['name'] = @user.fb_name || @user.goo_name || @user.user_name
        user['mobile'] = @user.mobile
        user['rating'] = @user.rating
        user['picture_url'] = @user.picture_url
        response['user'] = user  

        render json: {  success: true, message: "success", status_code: 200, data: response}
      end
  end
  