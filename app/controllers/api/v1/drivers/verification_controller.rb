class Api::V1::Drivers::VerificationController < ApplicationController
    before_action :authenticate_driver, only: [:upload_doc]

    def upload_doc
        if doc_params.present?
            begin
                @doc = DriverDoc.create(doc_params)
                #udpate id
                render json: {  success: true, message: "doc created successfully", status_code: 201, data: {}}
            rescue
                render json: {  success: false, message: "request failed", status_code: 500, data: {}}
            end
        end
    end

    private
  
    def doc_params
      params.permit(:aadhar_number,:aadhar_front,:aadhar_back,:dl_number,:dl_front,:dl_back,:pan_number,:pan_img)
    end
end
