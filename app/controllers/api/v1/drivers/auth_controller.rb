class Api::V1::Drivers::AuthController < ApplicationController
    before_action :authenticate_driver, only: []
    skip_before_action :verify_authenticity_token  

  
    def profile
      if driver_params.present?
        begin
          @driver = Driver.create(driver_params)
          @driver.update(active: true)
          render json: {  success: true, message: "Driver created successfully", new_user: true, status_code: 201, data: {session_token: @driver.session_token}}
        rescue
          render json: {  success: false, message: "Failed to create", status_code: 500, data: {}}
      end
    else
        render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
    end
   end
  
  
   def login
    if params[:mobile].present?
    @driver = Driver.where(mobile: params[:mobile])
      if @driver && @driver.authenticate(params[:password])
        render json: {  success: true, message: "Login success", new_user: false, status_code: 200, data: {session_token: @driver.session_token}}
      else
        render json: {  success: false, message: "Authentication failed", status_code: 412, data: {}}
      end
    else
      render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
    end
   end
  
   def verify_otp
    otp = '1234'
    if params[:mobile] && params[:otp]
      if otp == params[:otp]
        render json: {  success: true, message: "Otp verified", status_code: 200, data: {}}
    else
      render json: {  success: false, message: "Otp verification failed", status_code: 412, data: {}}
    end
  else
    render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
  end
  end

  def send_otp
    otp = '1234'
    if params[:mobile].present?
      @driver = Driver.where(mobile: params[:mobile])
      @mob = Otp.find_by(mobile: params[:mobile])
      if @mob.blank?
          begin
              @otp = Otp.create(mobile: params[:mobile], otp: otp)
              render json: {  success: true, message: "otp sent successfully", status_code: 200, data: {}}
            rescue
              render json: {  success: false, message: "Failed to send otp", status_code: 500, data: {}}
         end
      elsif @driver.blank?
        @mob.update(otp: otp)
        render json: {  success: true, message: "Resent Otp", status_code: 200, data: {}}
      else
        render json: {  success: true, message: "Driver already exists", status_code: 200, data: {}}
     end
   
    else
      render json: {  success: false, message: "Params missing", status_code: 400, data: {}}
    end
 end

  
  def forgot_password
    if params[:email].blank?
        render json: {error: "Email not present"}
      end
  
      @driver = Driver.find_by(cust_email: params[:email])
      if @driver.present?
        @driver.generate_password_token!
        PasswordResetMailer.reset_password_email(params[:email], @driver.reset_password_token).deliver_later
        render status: 200, json: {  message: "success"}
      else
        render status: 500,json: {  message: "Email not found"}
      end
  end
  
  def reset_password
      token = params[:token].to_s
  
      if params[:email].blank?
         render json: {message: "Token not present"}
      end
  
      @driver = driver.find_by(reset_password_token: token)
      if @driver.present? && @driver.password_token_valid?
        if @driver.reset_password!(params[:password])
          render status: 200, json: {  message: "success"}
        else
          render json: {  message: "Password Reset Failed"}
        end
      else
        render json: {  message: "Link not valid or expired. Try generating a new link"}
  
      end
    end
  
  private
    def gcm_params
      params.permit(:gcm_registration_id,:device_id)
    end
  
    def driver_params
      params.permit(:first_name,:last_name,:email,:password,:picture,:mobile)
    end
  
  
  end
  