class Api::V1::Drivers::DashboardController < ApplicationController
    before_action :authenticate_driver, only: [:location, :accept_booking]
    skip_before_action :verify_authenticity_token  
    include DriversHelper
  
      def location
        @drivers = set_drivers_redis(@current_driver.id, params[:lat], params[:long])
      end

      def accept_booking
        @booking = Booking.find_by_id(params[:booking_id])
        @booking.update(status: 1)
        render json: {  success: true, message: "Booking request accepted", status_code: 200, data: {}} 
      end
  end
  