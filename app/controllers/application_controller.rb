class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # after_filter :cors_set_access_control_headers
  # before_filter :cors_set_access_control_headers, :cors_preflight_check

  def authenticate_user
    if request.headers.env["HTTP_SESSION_TOKEN"].present?
    profile_token = request.headers.env["HTTP_SESSION_TOKEN"]
    @current_user = User.find_by_session_token(profile_token)
    render json: {  success: false, message: "Auth failed", status_code: 401, data: {}} if @current_user.nil?
    @current_user
    else
    @current_user = User.find_by_session_token(params[:session_token])
    @current_user
    end  
  end

  def current_user
    @current_user
  end

  def authenticate_driver
    if request.headers.env["HTTP_SESSION_TOKEN"].present?
    profile_token = request.headers.env["HTTP_SESSION_TOKEN"]
    @current_driver = Driver.find_by_session_token(profile_token)
    render json: {  success: false, message: "Auth failed", status_code: 401, data: {}} if @current_driver.nil?
    @current_driver
    else
    @current_driver = Driver.find_by_session_token(params[:session_token])
    @current_driver
    end  
  end

  def current_driver
    @current_driver
  end

  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
   headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
   headers['Access-Control-Request-Method'] = '*'
   headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
   end
   
   def cors_preflight_check
     if request.method == 'OPTIONS'
       headers['Access-Control-Allow-Origin'] = '*'
       headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
       headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version, Token'
       headers['Access-Control-Max-Age'] = '1728000'
   
       render :text => '', :content_type => 'text/plain'
     end
   end
end
