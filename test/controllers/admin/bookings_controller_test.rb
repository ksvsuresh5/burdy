require 'test_helper'

class Admin::BookingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_booking = admin_bookings(:one)
  end

  test "should get index" do
    get admin_bookings_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_booking_url
    assert_response :success
  end

  test "should create admin_booking" do
    assert_difference('Admin::Booking.count') do
      post admin_bookings_url, params: { admin_booking: {  } }
    end

    assert_redirected_to admin_booking_url(Admin::Booking.last)
  end

  test "should show admin_booking" do
    get admin_booking_url(@admin_booking)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_booking_url(@admin_booking)
    assert_response :success
  end

  test "should update admin_booking" do
    patch admin_booking_url(@admin_booking), params: { admin_booking: {  } }
    assert_redirected_to admin_booking_url(@admin_booking)
  end

  test "should destroy admin_booking" do
    assert_difference('Admin::Booking.count', -1) do
      delete admin_booking_url(@admin_booking)
    end

    assert_redirected_to admin_bookings_url
  end
end
