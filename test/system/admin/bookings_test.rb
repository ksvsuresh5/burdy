require "application_system_test_case"

class Admin::BookingsTest < ApplicationSystemTestCase
  setup do
    @admin_booking = admin_bookings(:one)
  end

  test "visiting the index" do
    visit admin_bookings_url
    assert_selector "h1", text: "Admin/Bookings"
  end

  test "creating a Booking" do
    visit admin_bookings_url
    click_on "New Admin/Booking"

    click_on "Create Booking"

    assert_text "Booking was successfully created"
    click_on "Back"
  end

  test "updating a Booking" do
    visit admin_bookings_url
    click_on "Edit", match: :first

    click_on "Update Booking"

    assert_text "Booking was successfully updated"
    click_on "Back"
  end

  test "destroying a Booking" do
    visit admin_bookings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Booking was successfully destroyed"
  end
end
